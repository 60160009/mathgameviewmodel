package com.example.plusgame

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import com.example.plusgame.databinding.ActivityMainBinding
import com.example.plusgame.databinding.FragmentPlusGameBinding
import kotlinx.android.synthetic.main.fragment_plus_game.*
import kotlin.random.Random

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [PlusGameFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PlusGameFragment : Fragment() {
    private lateinit var binding: FragmentPlusGameBinding
    private lateinit var plusGameViewModel: PlusGameViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding =  DataBindingUtil.inflate(inflater, R.layout.fragment_plus_game, container, false)
        plusGameViewModel = ViewModelProvider(this).get(PlusGameViewModel::class.java)
        binding.plusGameViewModel = plusGameViewModel

        plusGameViewModel.eventNextQuestion.observe(viewLifecycleOwner, Observer { hasNext ->
            Log.i("test","test")
            if(hasNext){
                enableAllButton()
                binding.invalidateAll()
                plusGameViewModel.onNextFinish()
            }

        })




        plusGameViewModel.eventHome.observe(viewLifecycleOwner, Observer { hasNext ->
            if(hasNext){
                view?.findNavController()?.navigate(R.id.action_plusGameFragment_to_startFragment)
                plusGameViewModel.changeToHomeFinish()
            }

        })
        plusGameViewModel.eventSelectChoice1.observe(viewLifecycleOwner, Observer { hasNext ->
            if(hasNext){
                disabledAllButton()
                binding.invalidateAll()
                plusGameViewModel.onSelectChoice1Finish()
            }

        })

        plusGameViewModel.eventSelectChoice2.observe(viewLifecycleOwner, Observer { hasNext ->
            if(hasNext){
                disabledAllButton()
                binding.invalidateAll()
                plusGameViewModel.onSelectChoice2Finish()
            }

        })

        plusGameViewModel.eventSelectChoice3.observe(viewLifecycleOwner, Observer { hasNext ->
            if(hasNext){
                disabledAllButton()
                binding.invalidateAll()
                plusGameViewModel.onSelectChoice3Finish()
            }

        })


       return binding.root
    }

    fun disabledAllButton(){
        binding.apply {
            button1.isEnabled =false
            button2.isEnabled =false
            button3.isEnabled =false
        }
    }
    fun enableAllButton(){
        binding.apply {
            button1.isEnabled =true
            button2.isEnabled =true
            button3.isEnabled =true
        }
    }


}