package com.example.plusgame

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.example.plusgame.databinding.ActivityMainBinding
import com.example.plusgame.databinding.FragmentMinusGameBinding
import com.example.plusgame.databinding.FragmentMulGameBinding
import com.example.plusgame.databinding.FragmentPlusGameBinding
import kotlinx.android.synthetic.main.fragment_plus_game.*
import kotlin.random.Random

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [PlusGameFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MulGameFragment : Fragment() {
    private lateinit var binding: FragmentMulGameBinding
    private lateinit var mulGameViewModel: MulGameViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding =  DataBindingUtil.inflate(inflater, R.layout.fragment_mul_game, container, false)
        mulGameViewModel = ViewModelProvider(this).get(MulGameViewModel::class.java)
        binding.mulGameViewModel = mulGameViewModel

        mulGameViewModel.eventNextQuestion.observe(viewLifecycleOwner, Observer { hasNext ->
            Log.i("test","test")
            if(hasNext){
                enableAllButton()
                binding.invalidateAll()
                mulGameViewModel.onNextFinish()
            }

        })




        mulGameViewModel.eventHome.observe(viewLifecycleOwner, Observer { hasNext ->
            if(hasNext){
                view?.findNavController()?.navigate(R.id.action_mulGameFragment_to_startFragment)
                mulGameViewModel.changeToHomeFinish()
            }

        })
        mulGameViewModel.eventSelectChoice1.observe(viewLifecycleOwner, Observer { hasNext ->
            if(hasNext){
                disabledAllButton()
                binding.invalidateAll()
                mulGameViewModel.onSelectChoice1Finish()
            }

        })

        mulGameViewModel.eventSelectChoice2.observe(viewLifecycleOwner, Observer { hasNext ->
            if(hasNext){
                disabledAllButton()
                binding.invalidateAll()
                mulGameViewModel.onSelectChoice2Finish()
            }

        })

        mulGameViewModel.eventSelectChoice3.observe(viewLifecycleOwner, Observer { hasNext ->
            if(hasNext){
                disabledAllButton()
                binding.invalidateAll()
                mulGameViewModel.onSelectChoice3Finish()
            }

        })


        return binding.root
    }

    fun disabledAllButton(){
        binding.apply {
            button1.isEnabled =false
            button2.isEnabled =false
            button3.isEnabled =false
        }
    }
    fun enableAllButton(){
        binding.apply {
            button1.isEnabled =true
            button2.isEnabled =true
            button3.isEnabled =true
        }
    }
}