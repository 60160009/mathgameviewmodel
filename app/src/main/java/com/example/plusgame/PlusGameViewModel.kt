package com.example.plusgame

import android.util.Log
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.Navigation
import androidx.navigation.Navigation.findNavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment.findNavController
import kotlin.random.Random

class PlusGameViewModel: ViewModel() {
    private val _score = MutableLiveData<Score>()
    val score: LiveData<Score>
        get() = _score

    private val _resultText = MutableLiveData<String>()
    val resultText: LiveData<String>
        get() = _resultText

    private val _num1 = MutableLiveData<Int>()
    val num1: LiveData<Int>
        get() = _num1

    private val _num2 = MutableLiveData<Int>()
    val num2: LiveData<Int>
        get() = _num2

    private val _choice1 = MutableLiveData<Int>()
    val choice1: LiveData<Int>
        get() = _choice1


    private val _choice2 = MutableLiveData<Int>()
    val choice2: LiveData<Int>
        get() = _choice2

    private val _choice3 = MutableLiveData<Int>()
    val choice3: LiveData<Int>
        get() = _choice3

    private val _sum = MutableLiveData<Int>()
    val sum: LiveData<Int>
        get() = _sum

    private val _eventSelectChoice1 = MutableLiveData<Boolean>()
    val eventSelectChoice1 : LiveData<Boolean>
        get() = _eventSelectChoice1

    private val _eventSelectChoice2 = MutableLiveData<Boolean>()
    val eventSelectChoice2 : LiveData<Boolean>
        get() = _eventSelectChoice2


    private val _eventSelectChoice3 = MutableLiveData<Boolean>()
    val eventSelectChoice3 : LiveData<Boolean>
        get() = _eventSelectChoice3


    private val _eventNextQuestion = MutableLiveData<Boolean>()
    val eventNextQuestion : LiveData<Boolean>
        get() = _eventNextQuestion

    private val _eventHome = MutableLiveData<Boolean>()
    val eventHome: LiveData<Boolean>
        get() = _eventHome

    fun createQuestion() {
        _num1.value = Random.nextInt(10) + 1
        _num2.value = Random.nextInt(10) + 1
        _sum.value = _num1.value!! + _num2.value!!

        val position: Int = Random.nextInt(3) + 1

        if (position == 1) {
            _choice1.value = sum.value
            _choice2.value = sum.value?.toInt()?.minus(2)
            _choice3.value = sum.value?.toInt()?.plus(2)
        } else if (position == 2) {
            _choice2.value = sum.value
            _choice1.value = sum.value?.toInt()?.minus(2)
            _choice3.value = sum.value?.toInt()?.plus(2)
        } else {
            _choice3.value = sum.value
            _choice2.value = sum.value?.toInt()?.minus(2)
            _choice1.value = sum.value?.toInt()?.plus(2)
        }
    }

    fun checkAnswer1(){

        Log.i("test", sum.value.toString() + " " + choice1.value)

        if (choice1.value == sum.value) {
            _resultText.value = "Correct"
           _score.value?.addCorrect()



        } else {
            _resultText.value = "Wrong"
          _score.value?.addWrong()


        }
        _eventSelectChoice1.value = true
    }
    fun onSelectChoice1Finish(){
        _eventSelectChoice1.value = false
    }
    fun checkAnswer2(){
        if (choice2.value == sum.value) {
            _resultText.value = "Correct"
            _score.value?.addCorrect()


        } else {
            _resultText.value = "Wrong"
            _score.value?.addWrong()
        }
        _eventSelectChoice2.value = true
    }
    fun onSelectChoice2Finish(){
        _eventSelectChoice1.value = false
    }
    fun checkAnswer3(){
        if (choice3.value == sum.value) {
            _resultText.value = "Correct"
            _score.value?.addCorrect()


        } else {
            _resultText.value = "Wrong"
            _score.value?.addWrong()

        }
        _eventSelectChoice3.value = true
    }
    fun onSelectChoice3Finish(){
        _eventSelectChoice1.value = false
    }
    fun next(){
        createQuestion()
        _resultText.value = "Please Select an Answer"
        _eventNextQuestion.value = true
    }
    fun onNextFinish(){
        _eventNextQuestion.value = false
    }

    fun changeToHome(){
        _eventHome.value = true
    }
    fun changeToHomeFinish(){
        _eventHome.value = false
    }
    init{
        createQuestion()
        _score.value = Score()
        _resultText.value = "Please Select an Answer"
    }
}